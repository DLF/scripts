#!/usr/bin/env bash

#######################################################################
# Links all  files (including hidden ones) from one directory to
# another one recursively.
#
# example:
#
# given a directory dir1 with some files and further directories:
#
# dir1 
# ├── .config
# │   ├── awesome
# │   │   ├── control.lua
#
# and given an empty directory dir2,
# calling "symlink_files_recursively dir1 dir2" will result in
# 
# dir2
# ├── .config
# │   ├── awesome
# │   │   │   ├── control.lua
#
# where all directories are created if they don't exist and
# where all files are symlinks to those in dir1.
#
# This of course also works if dir2 is not empty.
# If a files already exists in dir2,
# * it is overridden if it is a symlink
# * the script terminates with an error message if it is not a symlink
#
#######################################################################

from_dir=$1
to_dir=$2

###################################################
# the recursive function which does the actual work
###################################################

function _link_dot_dir {
    local dir="$1"
    local target_dir="$2"
    
    for entry in "$dir"/*
    do
        echo $entry
        local entry=${entry#$dir/}
        if [[ $entry == *"/"* ]]; then
            echo "Something went wrong. There's a slash in the dir or file name. Let's exit before something bad happens. :) Aborting."
            exit 1
        fi
        local target="$target_dir/$entry"
        local source="$dir/$entry"
        if [ -f "${source}" ] ; then
            if [[ -f "$target" && ! -L "$target" ]] ; then
                echo "Target $target exists but is no symlink. Will not override it. Aborting."
                exit 1
            fi
            ln -sfr "$source" "$target"
        elif [ -d "${source}" ] ; then
            if [[ ${entry} == .* ]]; then
                if [ -z $(echo "$entry" | sed s/\\.//g) ]; then
                    echo "Entry: $entry"
                    echo "Entered a self- or parent-dir. This should not happen. We exit here to avoid harming the system".
                    exit 1 
                fi
            fi
            echo "Entering $target."
            if [ ! -p "$target" ]; then
                mkdir -p "$target"
            fi
            _link_dot_dir "$source" "$target" 
            if [ $? -ne 0 ] ; then exit 1; fi
        else
            echo "Skipping ls entry $entry."
        fi
    done    
}

################
# Here we go...
################

set -e

if [ ! -d "$from_dir" ]; then
    echo "from dir does not exist. Aborting."
    exit 1
fi

if [ ! -d "$to_dir" ]; then
    echo "to dir does not exist. Aborting."
    exit 1
fi

cd "$from_dir"
from_dir=`pwd`
cd -

shopt -s dotglob
_link_dot_dir "$from_dir" "$to_dir"
result=$?
shopt -u dotglob
if [ $result -ne 0 ] ; then exit 1; fi

