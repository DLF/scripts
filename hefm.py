"""
HTML Extract From Mail

Get the HTML part from an e-mail.
"""
import email
from email import policy

with open("/home/daniel/temp/multipart.mail", "br") as f:
    msg = email.message_from_binary_file(f, policy=policy.EmailPolicy())

print(msg)