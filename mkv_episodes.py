#!/usr/bin/env python
from __future__ import annotations

import os
import unittest
import re
import sys


class FinalException(Exception):
    def __init__(self, msg):
        self.msg = msg


# stolen and adapted from https://code.activestate.com/recipes/577058/ 
def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is one of "yes" or "no".
    """
    valid = {"yes": "yes", "y": "yes", "ye": "yes",
             "no": "no", "n": "no"}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while 1:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return default
        elif choice in valid.keys():
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' " \
                             "(or 'y' or 'n').\n")


class Mapping:
    matchers = [
        r"Datei (\d+)\.mpls wurde als Titel (\d+) hinzugefügt *",  # German mpls
        r"Datei [^\d]*(\d+)\.m2ts wurde als Titel (\d+) hinzugefügt *",  # German mpls
    ]

    @staticmethod
    def _parse_makemkv_output(lines):
        mapping = {}
        for matcher in Mapping.matchers:
            for line in lines:
                match = re.match(matcher, line)
                if match:
                    groups = match.groups()
                    file_no = groups[0]
                    title_no = groups[1]
                    assert file_no not in mapping
                    mapping[int(file_no)] = int(title_no)
        return mapping

    def __init__(self, makemkv_output_file="makemkv.out"):
        with open(makemkv_output_file) as f:
            content = f.readlines()
        mapping = Mapping._parse_makemkv_output(content)
        if len(mapping) == 0:
            raise FinalException("Found no mapping information in the makemkv output file")
        self._mapping = mapping

    @property
    def file_no_to_title_no(self):
        return self._mapping


class Rename:
    def __init__(
        self,
        old_to_new_name: dict[str, str],
        missing_files: list[str],
        missing_title_nos: list[int]
    ):
        self.old_to_new_name = old_to_new_name
        self.missing_files = missing_files
        self.missing_title_nos = missing_title_nos


class MKVFileSet:
    matcher = r"[^_].*_t(\d*).mkv"
    name_pattern = "{ix:0>3}_{name}"

    @staticmethod
    def _parse_file_list(file_names):
        mapping = {}
        for file_name in file_names:
            match = re.match(MKVFileSet.matcher, file_name)
            if match:
                title_no = match.groups()[0]
                mapping[int(title_no)] = file_name
        return mapping

    def __init__(self):
        file_names = os.listdir()
        self._mapping = MKVFileSet._parse_file_list(file_names)

    @property
    def title_no_to_file_name(self):
        return self._mapping

    @staticmethod
    def _calc_raname_mapping(file_no_to_title_no: dict, title_no_to_file_name: dict) -> Rename:
        name_mapping = {}
        file_nos = list(file_no_to_title_no.keys())
        file_nos.sort()
        ix = 1
        missing_title_nos = []
        for file_no in file_nos:
            title_no = file_no_to_title_no[file_no]
            if title_no in title_no_to_file_name:
                file_name = title_no_to_file_name[title_no]
                new_name = MKVFileSet.name_pattern.format(ix=ix, name=file_name)
                ix += 1
                assert file_name not in name_mapping
                name_mapping[file_name] = new_name
            else:
                missing_title_nos.append(title_no)
        found_files = set(name_mapping.keys())
        all_files = set(title_no_to_file_name.values())
        missing_files = list(all_files - found_files)
        missing_files.sort()
        missing_title_nos.sort()
        return Rename(name_mapping, missing_files, missing_title_nos)

    def factor_rename_mapping(self, mkv_mapping: Mapping):
        return self._calc_raname_mapping(
            mkv_mapping.file_no_to_title_no,
            self.title_no_to_file_name
        )


if __name__ == "__main__":
    mapping = Mapping()
    mkv_file_list = MKVFileSet()
    rename = mkv_file_list.factor_rename_mapping(mapping)
    if len(rename.old_to_new_name) == 0:
        print("No mapping found.")
        exit(1)
    if len(rename.missing_files) > 0:
        print("For some files, no entry in the MakeMKV output could be found:")
        for name in rename.missing_files:
            print("    " + name)
    if len(rename.missing_title_nos) > 0:
        print("For some title numbers in the MakeMKV output, no file could be found:")
        print("    " + ", ".join([str(d) for d in rename.missing_title_nos]))
    print("Renaming:")
    for old, new in rename.old_to_new_name.items():
        print(old + "  →  " + new)
    print("\n")
    answer = query_yes_no("Rename now?")
    if answer == 'yes':
        for old, new in rename.old_to_new_name.items():
            os.rename(old, new)


class TestMKVFileList(unittest.TestCase):

    def test_file_name_mapping(self):
        file_names = [
            'Foo Season 1 Disc 1_t00.mkv',
            'Foo Season 1 Disc 1_t01.mkv',
            'Foo Season 1 Disc 1_t02.mkv',
            '_Foo Season 1 Disc 1_t03.mkv',
            'Foo Season 1 Disc 1_t04.mkv',
            'Foo Season 1 Disc 1_t05.mkv',
            'Foo Season 1 Disc 1_t06.mkv',
        ]
        expected = {
            0: 'Foo Season 1 Disc 1_t00.mkv',
            1: 'Foo Season 1 Disc 1_t01.mkv',
            2: 'Foo Season 1 Disc 1_t02.mkv',
            4: 'Foo Season 1 Disc 1_t04.mkv',
            5: 'Foo Season 1 Disc 1_t05.mkv',
            6: 'Foo Season 1 Disc 1_t06.mkv',
        }
        actual = MKVFileSet._parse_file_list(file_names)
        self.assertEqual(expected, actual)


class TestMapping(unittest.TestCase):

    def test_mapping_of_german_output(self):
        lines = [
            "Datei 00082.mpls wurde als Titel 19 hinzugefügt ",
            "Datei 00055.mpls wurde als Titel 20 hinzugefügt ",
            "Titel #00054.mpls hat eine Länge von 29 Sekunden und wurde übersprungen, da die minimale Titellänge 120 Sekunden beträgt",
            "Titel #00120.mpls hat eine Länge von 8 Sekunden und wurde übersprungen, da die minimale Titellänge 120 Sekunden beträgt",
            "Titel #00053.mpls hat eine Länge von 50 Sekunden und wurde übersprungen, da die minimale Titellänge 120 Sekunden beträgt",
            "Datei 00201.mpls wurde als Titel 21 hinzugefügt ",
            "Datei 00079.mpls wurde als Titel 23 hinzugefügt ",
        ]
        expected = {
            82: 19,
            55: 20,
            201: 21,
            79: 23
        }
        actual = Mapping._parse_makemkv_output(lines)
        self.assertEqual(expected, actual)
    
    def test_mapping_of_some_other_german_output(self):
        lines = [
            "Datei 00057.m2ts wurde als Titel 40 hinzugefügt",
        ]
        expected = {
            57: 40,
        }
        actual = Mapping._parse_makemkv_output(lines)
        self.assertEqual(expected, actual)

    def test_rename_mapping(self):
        title_no_to_file_name = {
            0: 'Foo Season 1 Disc 1_t00.mkv',
            8: 'Foo Season 1 Disc 1_t08.mkv',
            2: 'Foo Season 1 Disc 1_t02.mkv',
            1: 'Foo Season 1 Disc 1_t01.mkv',
        }
        file_no_to_title_no = {
            222: 0,
            333: 2,
            432: 10,
            123: 8,
        }
        expected_name_mapping = {
            'Foo Season 1 Disc 1_t00.mkv': '002_Foo Season 1 Disc 1_t00.mkv',
            'Foo Season 1 Disc 1_t08.mkv': '001_Foo Season 1 Disc 1_t08.mkv',
            'Foo Season 1 Disc 1_t02.mkv': '003_Foo Season 1 Disc 1_t02.mkv',
        }
        expected_missing_files = ["Foo Season 1 Disc 1_t01.mkv"]
        expected_missing_title_nos = [10]

        rename = MKVFileSet._calc_raname_mapping(file_no_to_title_no, title_no_to_file_name)

        self.assertEqual(rename.missing_files, expected_missing_files)
        self.assertEqual(rename.missing_title_nos, expected_missing_title_nos)
        self.assertEqual(rename.old_to_new_name, expected_name_mapping)
